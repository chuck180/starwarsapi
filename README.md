# Getting Started -> Starwars API

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Install Project

In the project directory, you can run:

### `git clone ssh-link`
navigate in the project folder
### `npm install`

## Run Project

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## About the Project

### The logic behind your design decisions

I used the fetch api to get all the data (Starwars API SWAPI -> https://swapi.dev/api/) \
I used Functional Components from React, because it supports static typing with typescript\
I used Bootstrap because its super simple and fast

### Any problems you faced

I encountered static typing errors typescript related as well some jsx related compiling issues

### How you would potentially improve the implementation if you had more time

Create React Hooks for the fetch calls\
Add dynamic characters with Pagination loading (currenty there are 10 static characters instead of 82 possible characters)\
Add more dynamic connections between more than two entities\
Also Date Time could be added with moment date object for example (some date fields are commented out at the moment)\
Remove Bootstrap and make use of Theme-UI

![Alt text](<src/img/starwars api.png>)
