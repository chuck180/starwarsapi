import React from 'react';
import './App.css';

import Oracle from './components/Oracle';

function App() {
  return (
    <div className="App">
      <Oracle />
    </div>
  );
}

export default App;
