import { FC } from "react";

import { PersonType } from '../types/Person';

const People: FC<PersonType> = ({
    name,
    height,
    mass,
    hair_color,
    skin_color,
    eye_color,
    birth_year,
    //gender,
    //homeworld,
    //films,
    //species,
    //vehicles,
    //starships,
    //created,
    //edited,
    //url,
}): JSX.Element => {
    return (
        <>
            <h5 className="card-title">{name}</h5>
            <hr></hr>
            <p><span className="text-info">Height:</span> {height} cm</p>
            <p><span className="text-info">Mass:</span> {mass} kg</p>
            <p><span className="text-info">Hair color:</span> {hair_color}</p>
            <p><span className="text-info">Skin color:</span> {skin_color}</p>
            <p><span className="text-info">Eye color:</span> {eye_color}</p>
            <p><span className="text-info">Birth year:</span> {birth_year}</p>
        </>

    )
};

export default People;