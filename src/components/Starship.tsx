import { FC } from "react";

import { StarshipType } from '../types/Starship';

const Starship: FC<StarshipType> = ({
    name,
    model,
    manufacturer,
    cost_in_credits,
    length,
    max_atmosphering_speed,
    crew,
    passengers,
    cargo_capacity,
    consumables,
    hyperdrive_rating,
    MGLT,
    starship_class,
    //pilots,
    //films,
    //created,
    //edited,
    //url,
}): JSX.Element => {
    return (
        <div className="card-body">
            <h5 className="card-title mb-3">Name: {name}</h5>
            <hr></hr>
            <p><span className="text-white">Model:</span> {model}</p>
            <p><span className="text-white">manufacturer:</span> {manufacturer}</p>
            <p><span className="text-white">Cost:</span> {cost_in_credits} Credits</p>
            <p><span className="text-white">Length:</span> {length} m</p>
            <p><span className="text-white">Speed:</span> {max_atmosphering_speed}</p>
            <p><span className="text-white">Crew:</span> {crew}</p>
            <p><span className="text-white">Passengers:</span> {passengers}</p>
            <p><span className="text-white">Cargo Capacity:</span> {cargo_capacity}</p>
            <p><span className="text-white">Consumables:</span> {consumables}</p>
            <p><span className="text-white">Hyperdrive Rating:</span> {hyperdrive_rating}</p>
            <p><span className="text-white">MGLT:</span> {MGLT}</p>
            <p><span className="text-white">Starship Class:</span> {starship_class}</p>
        </div>
    )
};

export default Starship;