import { FC } from "react";

const Navigation: FC<{
    title: string
}> = ({
    title
}): JSX.Element => {
        return (
            <nav className="navbar bg-black">
                <div className="container">
                    <a href="/" className="navbar-brand">
                        <img src={require('../img/Star_Wars_Logo.svg.webp')}
                            alt="starwars logo"
                            width="100" />
                    </a>
                    <p className="text-white">{title}</p>
                </div>
            </nav>
        )
    };

export default Navigation;