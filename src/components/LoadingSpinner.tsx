import { FC } from "react";

const LoadingSpinner: FC<{
    loadingText: string
}> = ({
    loadingText
}): JSX.Element => {
        return (
            <>
                <div className="spinner-grow text-light" role="status" style={{ marginRight: '20px', fontSize: '5px', height: '15px', width: '15px' }}>
                    <span className="visually-hidden">Loading...</span>
                </div>
                <span className="text-white" style={{ fontFamily: 'monospace', paddingBottom: '10px', fontSize: '17px' }}>Loading {loadingText}</span>
            </>
        )
    };

export default LoadingSpinner;