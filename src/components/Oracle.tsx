import { useEffect, useState } from "react";

import Navigation from './Navigation';
import Starship from "./Starship";
import People from './People';

import { PersonType } from '../types/Person';
import { StarshipType } from '../types/Starship';
import LoadingSpinner from "./LoadingSpinner";

function Oracle() {
    const [people, setPeople] = useState<PersonType | undefined>();
    const [starships, setStarships] = useState<StarshipType[] | undefined>();
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const characters = [
        { id: 1, name: "Luke Skywalker" },
        { id: 2, name: "C-3PO" },
        { id: 3, name: "R2-D2" },
        { id: 4, name: "Darth Vader" },
        { id: 5, name: "Leia Organa" },
        { id: 6, name: "Owen Lars" },
        { id: 7, name: "Beru Whitesun lars" },
        { id: 8, name: "R5-D4" },
        { id: 9, name: "Biggs Darklighter" },
        { id: 10, name: "Obi-Wan Kenobi" },
    ];

    const fetchPeople = async (id: number | undefined) => {
        setPeople(undefined);
        setIsLoading(true);
        const result = await fetch(`https://swapi.dev/api/people/${id}`)
            .then(checkStatus)
            .then(parseJSON)
            .catch(error => console.log('There was a problem in fetching People!', error))
            .then(ppl => {
                setStarships([]);
                setPeople(ppl);
                setIsLoading(false);
                console.log(people);
            })
    }

    const fetchStarships = async () => {
        setIsLoading(true)
        if (people && people.starships.length > 0) {
            Promise.all(people.starships.map(url =>
                fetch(url)
                    .then(checkStatus)
                    .then(parseJSON)
                    .catch(error => console.log('There was a problem in fetching Starships!', error))
            ))
                .then(data => {
                    console.log(data);
                    setStarships(data);
                    setIsLoading(false);
                })
        }
    }

    function checkStatus(response: any) {
        setIsLoading(true);
        if (response.ok) {
            return Promise.resolve(response);
        } else {
            setIsLoading(false);
            return Promise.reject(new Error(response.statusText));
        }
    }

    function parseJSON(response: any) {
        setIsLoading(false);
        return response.json();
    }

    useEffect(() => {

    }, [people, starships])

    return (
        <>
            <Navigation title={'Passing example title to React FC component'} />
            <div className="container">
                <div className="dropdown my-5">
                    <button className="btn btn-secondary dropdown-toggle"
                        type="button" id="dropdownMenuButton1"
                        data-bs-toggle="dropdown"
                        aria-expanded="false">
                        Choose Starwars Character
                    </button>
                    <ul className="dropdown-menu"
                        aria-labelledby="dropdownMenuButton1">
                        {characters.map((dropdownCharacter) => {
                            return (
                                <li key={dropdownCharacter.id} onClick={() => {
                                    fetchPeople(dropdownCharacter.id);
                                }}><a className="dropdown-item">{dropdownCharacter.name}</a></li>
                            )
                        })}
                    </ul>
                </div>

                {people ? (
                    <div className="card" style={{ width: '18rem' }}>
                        <div className="card-body">
                            <People
                                name={people.name}
                                height={people.height}
                                mass={people.mass}
                                hair_color={people.hair_color}
                                skin_color={people.skin_color}
                                eye_color={people.eye_color}
                                birth_year={people.birth_year}
                                starships={people.starships} />

                            {people.starships.length > 0 ? (
                                <>
                                    <hr></hr>
                                    <a onClick={() => fetchStarships()}
                                        className="btn btn-info">{people.starships.length} {people.starships.length === 1 ? 'Starship' : 'Starships'}</a>
                                </>
                            ) : (
                                <>
                                    <hr></hr>
                                    <span className="text-danger fw-semibold">Does not own a Starship</span>
                                </>
                            )}

                        </div>
                    </div>
                ) : <>
                    {isLoading && (
                        <LoadingSpinner loadingText="character" />
                    )}
                </>
                }

                {people && isLoading && people.starships.length > 0 && (
                    <div className="my-5">
                        <LoadingSpinner loadingText="starships" />
                    </div>
                )}

                {starships && people && (
                    <>
                        {people.starships.length > 0 ? (
                            <>
                                <div className="d-flex flex-row mb-3 my-5">
                                    {starships.map(function (starship, index) {
                                        return (
                                            <div key={index}
                                                className="card bg-info"
                                                style={{ width: '18rem', marginRight: '10px' }}>
                                                <Starship
                                                    name={starship.name}
                                                    model={starship.model}
                                                    manufacturer={starship.manufacturer}
                                                    cost_in_credits={starship.cost_in_credits}
                                                    length={starship.length}
                                                    max_atmosphering_speed={starship.max_atmosphering_speed}
                                                    crew={starship.crew}
                                                    passengers={starship.passengers}
                                                    cargo_capacity={starship.cargo_capacity}
                                                    consumables={starship.consumables}
                                                    hyperdrive_rating={starship.hyperdrive_rating}
                                                    MGLT={starship.MGLT}
                                                    starship_class={starship.starship_class} />
                                            </div>
                                        )
                                    })}
                                </div>
                            </>
                        ) : (
                            <></>
                        )}
                    </>
                )}
            </div>
        </>
    );
}

export default Oracle;